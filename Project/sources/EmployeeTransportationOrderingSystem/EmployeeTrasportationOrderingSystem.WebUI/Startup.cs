﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EmployeeTrasportationOrderingSystem.WebUI.Startup))]
namespace EmployeeTrasportationOrderingSystem.WebUI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
